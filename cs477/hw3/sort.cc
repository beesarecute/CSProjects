#include <algorithm>
#include <iostream>
#include <vector>
#include "sort.h"

int comparisons;

int main() {
  std::vector<char> sorted;
  std::vector<char> unsorted = {'E', 'A', 'S', 'Y', 'Q', 'U', 'E', 'S', 'T', 'I', 'O', 'N'};
  comparisons = 0;

  sorted = bs(unsorted);

  std::cout << "unsorted: ";
  print(unsorted);
  std::cout << "sorted:   ";
  print(sorted);
  std::cout << "comparisons: " << comparisons << std::endl;

  return 0;
}

/*
Desc: Calls helper functions to complete a full sort using bubblesort
Input: vector to sort
Output: None
Return: A sorted vector
*/
template <typename t>
std::vector<t> bs(std::vector<t> unsorted) {
  int start = 0;
  int end = unsorted.size() - 1;
  bool sorted = false;

  while( !sorted ) {
    sorted = left_pass(unsorted, start, end);
    if( sorted ) break;
    sorted = right_pass(unsorted, start, end);
  }

  return unsorted;
}

/*
Desc: Completes one iteration of bubblesort from left to right
      using the bounds start -> end
Input: vector to sort
       integer starting position
       integer ending position
Output: None
Return: True or false with respect to the sorted status
*/
template <typename t>
bool left_pass(std::vector<t> &unsorted, int &start, int &end) {
  int index;
  t min, max;
  bool sorted = true;

  min = max = unsorted[start];
  for( index = start; index < end; index++ ) {
    comparisons++;
    if( unsorted[index] > unsorted[index + 1] ) {
      if( unsorted[index] < min ) min = unsorted[index];
      if( unsorted[index] > max ) max = unsorted[index];
      std::swap(unsorted[index], unsorted[index + 1]);
      sorted = false;
    }
  }
  if( unsorted[start] == min ) start++;
  if( unsorted[end] == max ) end--;

  return sorted;
}

/*
Desc: Completes one iteration of bubblesort from right to left
      using the bounds end -> start
Input: vector to sort
       integer starting position
       integer ending position
Output: None
Return: True or false with respect to the sorted status
*/
template <typename t>
bool right_pass(std::vector<t> &unsorted, int &start, int &end) {
  int index;
  t min, max;
  bool sorted = true;

  min = max = unsorted[start];
  for( index = end; index > start; index-- ) {
    comparisons++;
    if( unsorted[index] < unsorted[index - 1] ) {
      if( unsorted[index] < min ) min = unsorted[index];
      if( unsorted[index] > max ) max = unsorted[index];
      std::swap(unsorted[index], unsorted[index - 1]);
      sorted = false;
    }
  }
  if( unsorted[start] == min ) start++;
  if( unsorted[end] == max ) end--;

  return sorted;
}

/*
Desc: Prints out a formatted vector
Input: vector to print
Output: Each element of the vector seperated by a space
Return: None
*/
template <typename t>
void print(std::vector<t> v) {
  unsigned int index;

  for( index = 0; index < v.size(); index++ ) {
    std::cout << v[index] << " ";
  }
  std::cout << std::endl;

  return;
}
