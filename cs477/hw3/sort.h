
template <typename t>
void print(std::vector<t> v);

template <typename t>
bool left_pass(std::vector<t> &unsorted, int &start, int &end);

template <typename t>
bool right_pass(std::vector<t> &unsorted, int &start, int &end);

template <typename t>
std::vector<t> bs(std::vector<t> unsorted);
