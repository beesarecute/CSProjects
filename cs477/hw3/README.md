### Description
This program implements a modified version of bubblesort. The differences from normal bubblesort are as follows:

1. It swaps elements just like regular bubblesort, only it does so in different passes. The algorithm alternates from left -> right passes to right -> left passes
2. Each iteration keeps track of whether or not the left and right most indices are in the right position and updates the sorting range respectively

### Compiling
To compile the code, open up a bash terminal and type **make**.

### Running
After compiling the code, the program can be ran from the same terminal by typing: **./sort**.
