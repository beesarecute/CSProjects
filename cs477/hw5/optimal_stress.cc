#include <algorithm>
#include <iostream>
#include <fstream>
#include <list>
#include <string>

// Structs
struct JobSchedule {
  int week_number;
  int low_stress;
  int high_stress;
  int selection;
};

// Prototypes
std::list<JobSchedule> read_file(std::string filename);
void print_schedule(std::list<JobSchedule> job_schedule);
int find_optimal(std::list<JobSchedule> job_schedule);
int find_optimal_helper(std::list<JobSchedule> job_schedule, std::list<JobSchedule>::iterator current);

// Main function
int main() {
  std::list<JobSchedule> job_schedule;
  int optimal_revenue;

  job_schedule = read_file("example_input.csv");
  print_schedule(job_schedule);
  std::cout << find_optimal(job_schedule) << std::endl;
  return 0;
}

// Read file function
std::list<JobSchedule> read_file(std::string filename) {
  std::ifstream fptr;
  JobSchedule temp_schedule;
  std::string buffer;
  std::list<JobSchedule> read_schedule;
  int week_counter = 0;

  fptr.open(filename);

  while( fptr.good() ) {
    week_counter++;
    temp_schedule.week_number = week_counter;
    getline(fptr, buffer, ',');
    if( buffer.empty() ) {
      break;
    }
    temp_schedule.low_stress = std::stoi(buffer);
    getline(fptr, buffer);
    temp_schedule.high_stress = std::stoi(buffer);
    temp_schedule.selection = -1;

    read_schedule.push_back(temp_schedule);
  }

  fptr.close();
  return read_schedule;
}

// Print schedule function
void print_schedule(std::list<JobSchedule> job_schedule) {
  std::list<JobSchedule>::iterator itr;

  for( itr = job_schedule.begin(); itr != job_schedule.end(); ++itr ) {
    std::cout << itr->week_number << ": " <<
                 itr->low_stress << ", " <<
                 itr->high_stress << std::endl;
  }
}

// Find optimal function
int find_optimal(std::list<JobSchedule> job_schedule) {
  if( job_schedule.size() == 0 ) {
    return 0;
  }
}

int find_optimal_helper(int week_number, int low_stress, int high_stress) {

}
