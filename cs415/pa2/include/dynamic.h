#ifndef DYNAMIC_H
#define DYNAMIC_H

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "mandelbrot.h"
#include "timer.h"

#define NUM_ROWS 100

#endif // DYNAMIC_H
