// dynamic.c
//
// Generates a mandelbrot set dynamically and outputs it to a .ppm

#include "../include/dynamic.h"

//*******************************//
// Creates mandelbrot set image based on user specifications
// Globals:
//   none
// Arguments:
//   number of command line arguments
//   list of command line arguments
// Returns:
//   0 :- upon successful generation
//   1 :- if an invalid number of arguments were supplied
//*******************************//
int main(int argc, char* argv[]) {
  int x_current, y_current;
  int world_rank, world_size;
  int width, height;
  int image_size;
  int index, n;
  int finish_code = -1;
  int processor_data[2]; // processor_data[0] == row number
                         // processor_data[1] == processor_id (aka world_rank)

  unsigned char red, green, blue;

  // verify that 2 extra arguments were supplied
    // the first should be the image size ( x = y )
    // the second should be the image to save to
  if( argc != 3 ) {
    fprintf(stderr, "main: invalid number of arguments\n");
    return 1;
  }

  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  if ( world_size <= 3 ) {
    fprintf(stderr, "main: world size is too small to run dynamically\n");
    return 1;
  }

  sscanf(argv[1], "%d", &width);
  sscanf(argv[1], "%d", &height);

  if( world_rank == 0 ) { // master node
    // assign tasks to nodes
    for ( index = 0; index < height; index += NUM_ROWS ) {
      // recv ready status from anyone
      MPI_Recv(&processor_data, 2, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // send that node a row number to start at
      MPI_Send(&index, 1, MPI_INT, processor_data[1], 0, MPI_COMM_WORLD);
    }

    // send finish_code to all slaves
    for( index = 2; index < world_size; index++ ) {
      MPI_Send(&finish_code, 1, MPI_INT, index, 0, MPI_COMM_WORLD);
    }

  } else if ( world_rank == 1 ) { // node that receives and builds the image
    double elapsed_time;
    struct timeval timer;
    char* image;
    FILE* fp;

    // start timer
    gettimeofday(&timer, NULL);

    // read in the image size
    image_size = width * height * 3;

    // allocate memory for image (x3 due to rgb)
    image = (char*) malloc (image_size);

    // rebuild image
    for ( index = 0; index < height; index += NUM_ROWS ) {
      MPI_Recv(&image[(index * height) * 3], 3 * width * NUM_ROWS, MPI_CHAR, MPI_ANY_SOURCE, index, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // end timer
    elapsed_time = get_time(timer);
    printf("%lf", elapsed_time);

    // open up the file specified to write the image to
    fp = fopen(argv[2], "wb");
    fprintf(fp,"P6\n%d %d\n255\n", width, height);

    // write image array to file
    for ( index = 0; index < image_size; index++ ) {
      fputc(image[index], fp);
    }

    // close file and free up allocations
    fclose(fp);
    free(image);
    image = NULL;

  } else { // remaining slaves
    image_size = 3 * width * NUM_ROWS;

    char temp_image[image_size];
    int count;

    processor_data[0] = 0;
    processor_data[1] = world_rank;

    while ( 1 ) {
      // send ready status (send id)
      MPI_Send(&processor_data, 2, MPI_INT, 0, 0, MPI_COMM_WORLD);

      // recv row #
      MPI_Recv(&processor_data, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // check if it was an exit code
      if ( processor_data[0] == finish_code ) {
        break;
      }

      count = 0;

      // calculate row(s)
      for ( y_current = processor_data[0]; y_current < processor_data[0] + NUM_ROWS; y_current++ ) {
        for ( x_current = 0; x_current < width; x_current++ ) {
          n = calculate_pixel(x_current, y_current, width, height);

          get_color(&red, &green, &blue, n);

          temp_image[count + 0] = red;
          temp_image[count + 1] = green;
          temp_image[count + 2] = blue;

          count+=3;
        }
      }

      // send rows to builder
      MPI_Send(&temp_image, image_size, MPI_CHAR, 1, processor_data[0], MPI_COMM_WORLD);

    }
  }

  MPI_Finalize();

  return 0;
}
