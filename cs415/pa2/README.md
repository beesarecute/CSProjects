# PA2: Mandelbrot

## Overview

This project takes a look at a problem that can be fairly parallel. The Mandelbrot set is calculated using both sequential methods, as well as parallel methods. Image sets are generated of sized upward of 400 million pixels. Times are then compared between the parallel functions and the sequential ones.

## Implementation

### Sequential
Sequential calculations are done on one processor and calculate one row at a time in order.

### Parallel
Parallel calculations are done dynamically. That is, processes are assigned rows to calculate, and when they finish, they are assigned more until the entire set has been calculated.

## Dependencies
This program relies on MPI and Slurm, both of which are installed on **h1.cse.unr.edu**.

## Building/Compiling
All of the necessary shared object files and executables can be created using the makefile provided. To do so, simply type:
```
$ cd build
$ make
```
This will create all of the necessary files in the **bin** folder. Conversely, to delete all of the compiled files, type:
```
$ cd build
$ make clean
```

## Running
To run the program, various sbatch files have been provided to help keep each test separate. The possible options are **sequential.sbatch**, and **dynamic.sbatch** (all of which are located in the build directory of the project). To run each of the jobs, the sbatch command should be used. E.g.
```
$ cd build
$ sbatch sequential.sbatch
```

## Results
The two batch files will record their timing results in the **log** directory. The images generated can be found in the **output** folder from this projects root directory. Data gathered can be found in the **data** directory, and a report that investigates the data can be viewed from the root directory inside **Report.pdf**.
