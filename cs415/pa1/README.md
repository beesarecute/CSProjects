# PA1: Ping-pong

## Overview

The main idea behind this project is to calculate the processing times of the compute cluster. To do so, we must be able to record the calculation times of jobs sent to processors on the same box as well as processors located on different nodes.

## Implementation

In order to best approach the task, a few libraries must first be implemented to best handle the problem. The problem itself can be separated into smaller sub-tasks to make it easier to attack. The primary sub-tasks are as follows:

### Timing
Timing is handled using MPI's built in **MPI_Wtime()**. Things become a little complicated because timers aren't necessarily in sync with each other. Because of that, timing is handled on a single node rather than across all nodes. Waits were also necessary in order to ensure that all of the nodes were progressing to each gate at the same time. In this case, a **MPI_Barrier()** was used to make the head node wait for the slave(s) to receive before recording the time.


### Communication
Communication is also handles using MPI by making use of some of the basic function available. More specifically, **MPI_Send()** was used on the master node to send messages to the slave node, and **MPI_Recv()** was used on the slave node to receive the messages sent. It is important to note that **MPI_Send()** doesn't follow the normal definition of "blocking". It can be more accurately described as *locally blocking* because it continues immediately after the send completes.

## Dependencies
This program relies on MPI and Slurm, both of which are installed on **h1.cse.unr.edu**.

## Building/Compiling
All of the necessary shared object files and executables can be created using the makefile provided. To do so, simply type:
```
$ cd build
$ make
```
This will create all of the necessary files in the **bin** folder. Conversely, to delete all of the compiled files, type:
```
$ cd build
$ make clean
```

## Running
To run the program, various sbatch files have been provided to help keep each test separate. The possible options are **One_box.sh**, **Two_box.sh**, and **Timing.sh** (all of which are located in the root directory of the project). To run each of the jobs, the sbatch command should be used. E.g.
```
$ cd build
$ sbatch One_box.sh
```

## Results
All three of the batch files will record their results in the **log** directory. Results from previous testing can be found in the **data** folder from this projects root directory. The data is in the form of excel spreadsheet containing 30,000 time recordings. A more detailed report can also be viewed from the root directory inside **Report.pdf**.