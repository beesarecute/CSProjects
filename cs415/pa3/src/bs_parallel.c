#include "../include/bs_parallel.h"

int main(int argc, char* argv[]) {
  int total_nums, partition_size, partition_remainder;
  int message_size;
  int world_size, world_rank;
  int index, bucket_index;
  int max;
  int* unsorted = NULL;
  double elapsed_time;
  struct timeval timer;
  Bucket big_bucket;
  Bucket* small_buckets = NULL;
  MPI_Status status;
  MPI_Request request;

  // confirm that the correct number of arguments were supplied
  if ( argc != 2 ) {
    fprintf(stderr, "ERROR: main(): invalid number of arguments supplied\n");
    return 1;
  }

  MPI_Init(NULL, NULL);

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // read in the size to sort
  sscanf(argv[1], "%d", &total_nums);

  // allocate memory for the unsorted array
  unsorted = (int *) calloc(total_nums, sizeof(*unsorted));

  // allocate memory for the small buckets
  small_buckets = (Bucket *) malloc(sizeof(Bucket) * world_size);

  // generate numbers to sort
  unsorted = rng(42, total_nums);

  // the maximum integer
  max = unsorted[0];
  for ( index = 1; index < total_nums; index++ ) {
    if ( unsorted[index] > max ) {
      max = unsorted[index];
    }
  }

  // size of big buckets
  partition_size = total_nums / world_size;


  if ( world_rank == 0 ) { // master code
    // BEGIN LOCAL CALCULATION PHASE
    // master bucket takes care of any leftovers
    partition_remainder = total_nums - (partition_size * (world_size - 1));
    bucket_index = 0;

    // initialize the big bucket
    big_bucket.size = 0;
    big_bucket.nums = (int *) calloc(total_nums, sizeof(int));

    // initialize the small buckets
    for ( bucket_index = 0; bucket_index < world_size; bucket_index++ ) {
      small_buckets[bucket_index].size = 0;
      small_buckets[bucket_index].nums = (int *) calloc(partition_remainder, sizeof(int));
    }

    // read in numbers to the big bucket
    for ( index = 0; index < partition_remainder; index++ ) {
      big_bucket.nums[index] = unsorted[(world_size - 1) * partition_size + index];
      big_bucket.size++;
    }

    // add in numbers to smaller buckets
    partition_remainder = max / world_size;
    for ( index = 0; index < big_bucket.size; index++ ) {
      for ( bucket_index = 0; bucket_index < world_size; bucket_index++ ) {
        if ( big_bucket.nums[index] < (bucket_index + 1) * partition_remainder ) {
          small_buckets[bucket_index].nums[small_buckets[bucket_index].size] = big_bucket.nums[index];
          small_buckets[bucket_index].size++;
          break;
        } else if ( bucket_index == world_size - 1 ) {
          small_buckets[bucket_index].nums[small_buckets[bucket_index].size] = big_bucket.nums[index];
          small_buckets[bucket_index].size++;
        }
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    // END LOCAL CALCULATION PHASE

    // begin timer
    gettimeofday(&timer, NULL);

    // BEGIN BUCKET DISTRIBUTING PHASE
    // reset big bucket in order to store incoming data
    big_bucket.size = 0;

    // send out all data that doesn't belong on this node
    for ( index = 0; index < world_size; index++ ) {
      if ( index != world_rank ) {
        MPI_Isend(small_buckets[index].nums, small_buckets[index].size, MPI_INT, index, 0, MPI_COMM_WORLD, &request);

        MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &message_size);

        MPI_Recv(&big_bucket.nums[big_bucket.size], message_size, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        big_bucket.size += message_size;
        MPI_Wait(&request, &status);
      }
    }
    // END BUCKET DISTRIBUTING PHASE

    // begin sorting
    qsort(big_bucket.nums, big_bucket.size, sizeof(int), &intcmp);

    // wait for others to finish
    MPI_Barrier(MPI_COMM_WORLD);

    // end timer
    elapsed_time = get_time(timer);

    // print time taken
    printf("%lf", elapsed_time);

  } else { // slave code
    // BEGIN LOCAL CALCULATION PHASE
    bucket_index = 0;

    // initialize big bucket
    big_bucket.size = 0;
    big_bucket.nums = (int *) calloc(total_nums, sizeof(int));

    // initialize the small buckets
    for ( bucket_index = 0; bucket_index < world_size; bucket_index++ ) {
      small_buckets[bucket_index].size = 0;
      small_buckets[bucket_index].nums = (int *) calloc(partition_size, sizeof(int));
    }

    // read in the numbers to the big bucket
    for ( index = 0; index < partition_size; index++ ) {
      big_bucket.nums[index] = unsorted[(world_rank - 1) * partition_size + index];
      big_bucket.size++;
    }

    // add in numbers to smaller buckets
    partition_size = max / world_size;
    for ( index = 0; index < big_bucket.size; index++ ) {
      for ( bucket_index = 0; bucket_index < world_size; bucket_index++ ) {
        if ( big_bucket.nums[index] < (bucket_index + 1) * partition_size ) {
          small_buckets[bucket_index].nums[small_buckets[bucket_index].size] = big_bucket.nums[index];
          small_buckets[bucket_index].size++;
          break;
        } else if ( bucket_index == world_size - 1 ) {
          small_buckets[bucket_index].nums[small_buckets[bucket_index].size] = big_bucket.nums[index];
          small_buckets[bucket_index].size++;
        }
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    // END LOCAL CALCULATION PHASE

    // BEGIN BUCKET DISTRIBUTING PHASE
    // reset big bucket in order to store incoming data
    big_bucket.size = 0;

    // begin sending and receiving
    for ( index = 0; index < world_size; index++ ) {
      if ( index != world_rank ) {
        MPI_Isend(small_buckets[index].nums, small_buckets[index].size, MPI_INT, index, 0, MPI_COMM_WORLD, &request);

        MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &message_size);

        MPI_Recv(&big_bucket.nums[big_bucket.size], message_size, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        big_bucket.size += message_size;
        MPI_Wait(&request, &status);
      }
    }
    // END BUCKET DISTRIBUTING PHASE

    // begin sorting
    qsort(big_bucket.nums, big_bucket.size, sizeof(int), &intcmp);

    MPI_Barrier(MPI_COMM_WORLD);

  }

  for ( bucket_index = 0; bucket_index < world_size; bucket_index++ ) {
    free(small_buckets[bucket_index].nums);
    small_buckets[bucket_index].nums = NULL;
  }
  free(small_buckets);
  small_buckets = NULL;

  free(unsorted);
  unsorted = NULL;

  MPI_Finalize();

  return 0;
}

int intcmp(const void* first, const void* second) {
    int a = *((int*) first);
    int b = *((int*) second);

    if ( a == b ) {
      return 0;
    } else if ( a < b ) {
      return -1;
    } else {
      return 1;
    }
}

