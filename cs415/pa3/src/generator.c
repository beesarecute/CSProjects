#include "../include/generator.h"

int* rng(int seed, int count) {
	int index;
	int* random_numbers;

	random_numbers = (int *) calloc(count, sizeof(*random_numbers));
	srand(seed);

	for ( index = 0; index < count; index++ ) {
		random_numbers[index] = rand() % MAX_SIZE;
	}

 	return random_numbers;
}
