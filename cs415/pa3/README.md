# PA3: Bucket Sorting

## Overview

## Implementation

### Sequential
One processor is in charge of both scattering and gathering to and from itself (or more specifically, its "buckets").

### Parallel
Varying amounts of processors split up the unsorted array before sorting and then return them after sorting. The amount of buckets used is dependent on the size of the system supplied.

## Dependencies
All of the non-sequential programs rely on MPI and Slurm, both of which are installed on **h1.cse.unr.edu**.

## Building/Compiling
All of the necessary shared object files and executables can be created using the makefile provided. To do so, simply type:
```
$ cd build
$ make
```
This will create all of the necessary files in the **bin** folder. Conversely, to delete all of the compiled files, type:
```
$ cd build
$ make clean
```

## Running
To run the program, various sbatch files have been provided to help keep each test separate. The possible options are **bs_sequential.sbatch**, and **bs_parallel1.sbatch** - **bs_parallel4.sbatch** (all of which are located in the build directory of the project). To run each of the jobs, the sbatch command should be used. E.g.
```
$ cd build
$ sbatch sequential.sbatch
```

## Results
The two batch files will record their timing results in the **log** directory. Data gathered can be found in the **data** directory, and a report that investigates the data can be viewed from the root directory inside **Report.pdf**.
