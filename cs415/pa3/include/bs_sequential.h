#ifndef BS_SEQUENTIAL_H
#define BS_SEQUENTIAL_H

#include <stdio.h>
#include <stdlib.h>

#include "generator.h"
#include "timer.h"

#define NUM_BUCKETS 10

typedef struct Bucket {
  int size;
  int* nums;
} Bucket;

int intcmp(const void* first, const void* second);
void scatter(Bucket* buckets, int* unsorted, int size, int max);
void bs_sort(Bucket* buckets);
void gather(Bucket* buckets, int* sorted);

#endif