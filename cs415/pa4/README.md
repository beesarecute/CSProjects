# PA4: Matrix Multiplication

## Overview
Matrix multiplication is a n^3 algorithm that can be very time consuming. Because of this, an efficient parallel algorithm is desirable.

## Implementation

### Sequential
Sequentially, multiplying two matrices together requires that one core calculates every individual element of the product matrix. To do so, the program must iterate through every row and column that exists, as well as iterate through each of the products required to find the total summed result.

### Parallel
The parallel side of it takes advantage of Cannon's algorithm to greatly reduce the computation time required for the matrix multiplication. 

## Dependencies
All of the non-sequential programs rely on MPI and Slurm, both of which are installed on **h1.cse.unr.edu**. If MPI isn't installed, the project will fail to build. If slurm isn't installed, the project will fail to run given the methods provided.

The program requires that at least one file exists that contains a matrix in it. The file should be formatted to have the first line specifying the size of the matrix, and the following lines being the rows of said matrix.

## Building/Compiling
All of the necessary shared object files and executables can be created using the makefile provided. To do so, simply type:
```
$ cd build
$ make
```
This will create all of the necessary files in the **bin** folder. Conversely, to delete all of the compiled files, type:
```
$ cd build
$ make clean
```

## Running
To run the program, follow the build instructions and then type:
```
$ sbatch sequential.sbatch
```
or
```
$ sbatch parallel.sbatch
```

Custom parallel jobs can be ran using srun as long as two files containing matrices are supplied in the command line arguments.

## Results
The two batch files will record their timing results in the **log** directory. Data gathered can be found in the **data** directory, and a report that investigates the data can be viewed from the root directory inside **Report.pdf**. Verbose output can be switched on and off by changing the PRINT macro in **matrix_parallel.h** to 1 and 0, respectively.
