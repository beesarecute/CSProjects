#include <stdio.h>

void  new_grade(int grades[5]);
float calculate_average(int grades[5]);
void  clear_grades(int grades[5]);
int   count_grades(int grades[5]);
int   grade_report(int grades[5]);
int flush_buff();

int main()
{
  int input;
  int grades[5] = {0, 0, 0, 0, 0};
  
  printf("This program maintains a class grade sheet. Options:\n");

  do
  {
    printf("1 – enter new grade\n");
    printf("2 – print current average\n");
    printf("3 – clear grade sheet\n");
    printf("4 – print total number of grades\n");
    printf("5 – print grade report\n");
    printf("6 – quit the program\n");
    printf("Input: ");

    scanf("%d", &input);
    switch(input)
    {
      case 1:   new_grade(grades);          break;
      case 2:   calculate_average(grades);  break;
      case 3:   clear_grades(grades);       break;
      case 4:   count_grades(grades);       break;
      case 5:   grade_report(grades);       break;
      case 6:   printf("Exiting...\n");     break;
      default:  printf("Invalid input.\n"); flush_buff();
    }
  } while ( input != 6 );

  return 0;
}

// function takes an integer array as a parameter
// this array represents the possible letter grades A-F
// the function will then update the array to reflect the grade entered
void new_grade(int grades[5])
{
  int valid_input;
  char input;

  do
  {
    printf("Enter a new grade (allowed values: A, B, C, D, F): ");
    scanf(" %c", &input);
    switch(input)
    {
      case 'A': grades[4]++; valid_input = 1; break;
      case 'B': grades[3]++; valid_input = 1; break;
      case 'C': grades[2]++; valid_input = 1; break;
      case 'D': grades[1]++; valid_input = 1; break;
      case 'F': grades[0]++; valid_input = 1; break;
      default: printf("Invalid input.\n"); valid_input = 0; flush_buff();
    }
  } while( !valid_input );
  
  return;
}

// function takes integer array as parameter
// calculated average of total grades stored in the array
// returns this average value and prints it out
float calculate_average(int grades[5])
{
  float average = 0;
  int total_grades = 0;
  
  for( int i = 0; i < 5; i++ )
  {
    average += (grades[i] * i);
    total_grades += grades[i];
  }
  
  if( total_grades != 0 )
  {
    average /= total_grades;
    printf("Current average is: %f\n", average);
  }
  else
  {
    printf("No grades have yet been entered.\n");
  }
  
  return average;
}

// clears the integer array and notifies user upon completion
void clear_grades(int grades[5])
{
  for( int i = 0; i < 5; i++ )
  {
    grades[i] = 0;
  }
  
  printf("Grade sheet has been cleared.\n");
  
  return;
}

// counts total grades entered by summing each element of grade array
// returns this sum and prints it out
int count_grades(int grades[5])
{
  int total_grades = 0;
  
  for( int i = 0; i < 5; i++ )
  {
    total_grades += grades[i];
  }
  
  printf("Current number of grades entered is: %d\n", total_grades);
  
  return total_grades;
}

// prints the tally of grades entered at specified subset
// the value is also returned
int grade_report(int grades[5])
{
  int count;
  char input;
  
  printf("Enter the grade for the report (allowed values: A, B, C, D, F): ");
  scanf(" %c", &input);
  
  switch(input)
  {
    case 'A': count = grades[4]; break;
    case 'B': count = grades[3]; break;
    case 'C': count = grades[2]; break;
    case 'D': count = grades[1]; break;
    case 'F': count = grades[0]; break;
    default: printf("Invalid input.\n"); flush_buff(); count = -1;
  }
  
  if( count != -1 )
  {
    printf("Total number of %c grades is: %d\n", input, count);
  }
  
  return count;
}

int flush_buff()
{
  int count = 0;
  char c;

  while( (c = getchar()) != '\n' && c != EOF )
  {
    count++;
  }

  return count;
}
