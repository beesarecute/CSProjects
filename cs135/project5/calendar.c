#include <stdio.h>

void print_month(int month, char day);
char next_month_first_day(int month, char day);
//Name: Poop
int main()
{
    int month, months;
    char day;

    printf("This program prints a formatted calendar\n");

    // ask user for starting month
    printf("Enter the starting month: ");
    scanf("%d", &month);

    // ask user for starting day of specified month
    switch( month )
    {
      case 1:  printf("What is the 1st day of January: ");   break; 
      case 2:  printf("What is the 1st day of February: ");  break; 
      case 3:  printf("What is the 1st day of March: ");     break; 
      case 4:  printf("What is the 1st day of April: ");     break; 
      case 5:  printf("What is the 1st day of May: ");       break; 
      case 6:  printf("What is the 1st day of June: ");      break; 
      case 7:  printf("What is the 1st day of July: ");      break; 
      case 8:  printf("What is the 1st day of August: ");    break; 
      case 9:  printf("What is the 1st day of September: "); break; 
      case 10: printf("What is the 1st day of October: ");   break; 
      case 11: printf("What is the 1st day of November: ");  break; 
      case 12: printf("What is the 1st day of December: ");  break; 
      default: printf("You broke it. Congrats.\n");          return 1; 
    }
    scanf(" %c", &day);
    
    // ask user for number of months to print
    printf("How many months do you want printed: ");
    scanf("%d", &months);
    
    // loop the scanned number of times
    for( int i = 0; i < months; i++ )
    {
      // print the month
      print_month(month, day);
      
      //get the start day of the next month
      day = next_month_first_day(month, day);
      
      // increment the month number, wrapping around if necessary
      month = (month + 1) % 13;
      
      // since valid numbers are 1-12, we will have to increment the month
      // if it is 0
      if( month == 0 )
        month = 1;
    }
    return 0;
}

// This function takes a month as an integer and a starting day as a character
// It will then print out the formatted month using the start day specified 
void print_month(int month, char day)
{
    int days; 
    int index = 0;
    char dow[7] = {'s', 'm', 't', 'w', 'r', 'f', 'a'};
    
    printf("\n");
    switch( month )
    {
      case 1:  printf("January\n");   days = 31; break; 
      case 2:  printf("February\n");  days = 28; break; 
      case 3:  printf("March\n");     days = 31; break; 
      case 4:  printf("April\n");     days = 30; break; 
      case 5:  printf("May\n");       days = 31; break; 
      case 6:  printf("June\n");      days = 30; break; 
      case 7:  printf("July\n");      days = 31; break; 
      case 8:  printf("August\n");    days = 31; break; 
      case 9:  printf("September\n"); days = 30; break; 
      case 10: printf("October\n");   days = 31; break; 
      case 11: printf("November\n");  days = 30; break; 
      case 12: printf("December\n");  days = 31; break;
      default: printf("Can't print month #%d\n", month); 
    }
    printf(" S  M  T  W  T  F  S\n");
    
    while( day != dow[index] )
    {
      printf("   ");
      index++;
    }
    
    for( int i = 1; i <= days; i++ )
    {
      if( index > 6 )
      {
        printf("\n");
        index = 0;
      }
      printf("%2d ", i);
      index++;
    }
    printf("\n");
}

// This function takes in a month as an integer and a day as a character
// It will return the character of the day that the next month should start on
char next_month_first_day(int month, char day)
{
  int days;
  int day_num = 0;
  int dow[7] = {'s', 'm', 't', 'w', 'r', 'f', 'a'};
  
  switch( month )
  {
    case 1:  days = 31; break; 
    case 2:  days = 28; break; 
    case 3:  days = 31; break; 
    case 4:  days = 30; break; 
    case 5:  days = 31; break; 
    case 6:  days = 30; break; 
    case 7:  days = 31; break; 
    case 8:  days = 31; break; 
    case 9:  days = 30; break; 
    case 10: days = 31; break; 
    case 11: days = 30; break; 
    case 12: days = 31; break; 
  }

  while( day != dow[day_num] )
  {
    day_num++;
  }
  
  return dow[ ((day_num + days) % 7) ];
}
