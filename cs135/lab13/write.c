#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
  FILE *fp;

  int age;
  int name_len;
  int index = 0;

  char ch;
  char name[100];
  char age_str[100];

  // open file with write permissions
  fp = fopen( argv[1] , "w" );
  
  printf("This program reads and saves ages and names in a binary file.\n");

  // loop forever
  do {
    printf("Enter person (age, name): ");
    ch = getchar();
    
    // if line is blank, exit loop
    if ( ch == '\n' )
    {
      break;
    }
    else
    {
      // get age
      while( ch != ',' )
      {
        age_str[index] = ch;
        index++;
        ch = getchar();
      }
      age = atoi(age_str);      
      index = 0;

      // get name
      ch = getchar();
      while( ch != '\n' )
      {
        name[index] = ch;
        index++;
        ch = getchar();
      }
      
      // get length of name
      name_len = strlen(name);
      
      // write data to file
      fwrite(&age, 1, sizeof(int), fp);    
      fwrite(&name_len, 1, sizeof(int), fp);    
      fwrite(name, 1, sizeof(name), fp);          
    }
  } while ( 1 );

  // close file
  fclose(fp);
  
  return 0;
}
