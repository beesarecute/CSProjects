% Reverse list function (#1) ------------------------------------------------- %
reverse([], []).

reverse([H|T], L) :- 
  reverse(T, Reversed_list),
  append(Reversed_list, [H], L). 
% ---------------------------------------------------------------------------- %

% Take from list function (#2) ----------------------------------------------- %
take(_, 0, []).

take([], _, []).

take([H|L], N, [H|L1]) :- 
  N > 0, 
  X is N-1, 
  take(L, X, L1).
% ---------------------------------------------------------------------------- %

% Binary tree functions (#3) ------------------------------------------------- %

% Definition of Binary tree (used in all subsequent definitions)
tree(nil).

tree(node(_, L, R)) :- 
  tree(L), 
  tree(R).

% Function nleaves(T,N) that returns true if N is the number of leaves in T (#3a)
nleaves(nil, 0).

nleaves(node(_, nil, nil), 1).

nleaves(node(_, L, R), N) :-
  nleaves(L, NewL),
  nleaves(R, NewR),
  N is NewL + NewR.

% Function treeMember(E,T) that returns true if element E is in the Tree T (#3b)
treeMember(E, node( E, _L, _R)).

treeMember(E, node(_E1, L, _R)) :-
  treeMember(E, L).

treeMember(E, node(_E1, _L, R)) :-
  treeMember(E, R).

% Function preOrder(T,L) that returns true if L is the pre-order traversal of T (#3c)
preOrderH(nil) --> [].

preOrderH(node(V, L, R)) -->
  [V],
  preOrderH(L),
  preOrderH(R).

preOrder(V, NewL) :- 
  phrase(preOrderH(V), NewL).

% Function height(T,N) that returns true if N is the height of tree T (#3d)
height(nil, 0).

height(node(_, nil, nil), 1).

height(node(_, L, R), N) :-
  height(L, N1),
  height(R, N2),
  N is max(N1, N2) + 1.
% ---------------------------------------------------------------------------- %

% Insert into list function (#4) ------------------------------------------------- %
insert( X, [], [X] ).

insert( X, [H|T], C ) :- 
  H =< X, 
  insert( X, T, C1 ), 
  append( [H], C1, C ).

insert( X, [H|T], C ) :- H > X, append( [X, H], T, C ).
% ---------------------------------------------------------------------------- %

% Flatten list function (#5) ------------------------------------------------- %
flatten([],[]).

flatten( [H|T], B ) :- 
  flatten( H, Flattened_H ), 
  flatten( T, Flattened_T ), 
  append( Flattened_H, Flattened_T, B ).
  
flatten(H, [H]).
% ---------------------------------------------------------------------------- %
