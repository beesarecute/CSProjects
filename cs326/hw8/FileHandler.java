package ColorSampler;

import java.io.*;
import java.util.*;

class FileHandler
{
  public int CountLines(String filename)
  {
    int lines = 0;
    try {
      BufferedReader reader = new BufferedReader(new FileReader(filename));
      while( reader.readLine() != null ) lines++;
      reader.close();      
    }
    catch( IOException e ) {
      e.printStackTrace();
    }
    
    return lines;
  }
  
  public ColorData[] ReadColorData(String filename)
  {
    int lines = CountLines(filename);
    ColorData[] cData = new ColorData[lines];

    try {
      BufferedReader reader = new BufferedReader(new FileReader(filename));
      Scanner scanner = new Scanner(reader);

      for( int index = 0; index < lines; index++ )
      {
        cData[index] = new ColorData();
        cData[index].SetColor(scanner.next()); 
        cData[index].SetRVal(scanner.nextInt());
        cData[index].SetGVal(scanner.nextInt());
        cData[index].SetBVal(scanner.nextInt());
      }
      reader.close();      
    }
    catch( IOException e ) {
      e.printStackTrace();
    }
    
    return cData;
  }

  public void SaveColorData(ColorData[] cData, String filename)
  {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

      for( int index = 0; index < cData.length; index++ )
      {
        writer.write(cData[index].GetColor() + " " + cData[index].GetRVal() + " " + cData[index].GetGVal() + " " + cData[index].GetBVal() + "\n");
      }
      writer.close();      
    }
    catch( IOException e ) {
      e.printStackTrace();
    }
    
  }

}