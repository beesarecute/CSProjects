#include "memory.h"

// Function to find next open block of memory
// Input: 	System configuration to determine memory layout
//					Int representing requested memory in kbytes
// Outcome: An unsigned int will be returned representing the
//					next available memory location
// Notes:		If the next available block exceeds the max system memory,
//					the location returned will start back at 0
//
unsigned int AllocateMemory(config *system, int requested_memory)
{
	unsigned int address;

	if( ((*system).block_size * (*system).allocated_blocks) > (*system).physical_memory )
	{
		(*system).allocated_blocks = 1;
		return 0;
	}
	
	address = requested_memory * (*system).allocated_blocks;
	(*system).allocated_blocks++;

	return address;
}

// Function to convert a decimal input to hexidecimal
// Input: 	Unsigned integer decimal number
// Outcome: The hexadecimal equavilent to the input will be returned
// Notes: 	This function intentionally doesn't calculate inputs that aren't
//					divisible by 8. 
//
//					This function is deprecated as of version 1.1
//
unsigned int decimal_to_hex(unsigned int input)
{
	int quotient;
	int remainder;
	int dividend = input;
	char hex[8];
	unsigned int hex_string = 0;
	
	if( input > 9 && (input % 8) != 0 )
	{
		return 0;
	}
	
	for( int index = 7; index >= 0; index-- )
	{
		quotient = dividend / 16;
		remainder = dividend % 16;
		dividend = quotient;
		hex[index] = remainder;
	}
	for( int index = 0; index < 8; index++ )
	{
		hex_string += hex[index] * power(10, (7 - index));
	}
	
	return hex_string;
}