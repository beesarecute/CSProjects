#ifndef FILE_H_
#define FILE_H_
#define MAX_LEN 255

int get_line(FILE *stream, char *line, char delim);
int file_exists();

#endif // FILE_H_
