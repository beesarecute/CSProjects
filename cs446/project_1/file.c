#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "file.h"


// function to get a line from a file
int get_line(FILE *stream, char *line, char delim)
{
  int index = 0;
  memset(line, 0, MAX_LEN); // flush array

  line[index] = getc(stream); // get character at cursor

  // check if we are already at end of file
  if( line[index] == EOF )
  {
    return -1;
  }

  // continue grabbing characters until one of the following occurs:
  //   * character limit is reached
  //   * specified delimeter is hit
  //   * newline character is hit
  while( index < MAX_LEN && line[index] != delim && line[index] != '\n')
  {
    index++;
    line[index] = getc(stream);

    if( line[index] == EOF )
    {
      line[index] = '\0'; // drop the stored EOF character
      break; // break to conserve current string
    }
  }

  return 0;
}

// function to check if a specified file exists
int file_exists(char filename[MAX_LEN])
{
  if( access( filename, F_OK ) != -1 )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}
