#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"

#define MAX_LEN 255 // set max string length

typedef struct
{
  int processor_cycle_time;
  int monitor_display_time;
  int hard_drive_cycle_time;
  int printer_cycle_time;
  int keyboard_cycle_time;
  int memory_cycle_time;

  int physical_memory;

  float version;
  char meta_file_path[MAX_LEN];
  char log_type[MAX_LEN];
  char log_path[MAX_LEN];
} config;

typedef struct
{
  char code;
  char descriptor[MAX_LEN];
  int cycles;
} meta_data;

config read_config(char filename[MAX_LEN]);
void read_data(meta_data *data, char filename[MAX_LEN]);
char get_code(char* string);
int get_descriptor(char* string, char* descriptor);
int get_cycles(char* string);
int power(int base, int power);

void output_to_log(config configuration, meta_data *mdata);
void output_to_monitor(config configuration, meta_data *mdata);

int main(int argc, char* argv[])
{
  config main_config;
  meta_data mdata[100];

  // check that the program was ran correctly
  if( argc != 2 )
  {
    printf("[ERROR] Invalid number of arguments given. Exit 1.\n");
    return 1;
  }

  // check that the file specified exists
  if( !file_exists(argv[1]) )
  {
    printf("[ERROR] Configuration file does not exist. Exit 1.\n");
    return 1;
  }

  // read in the configuration file
  main_config = read_config(argv[1]);

  // read in the meta data
  read_data(mdata, main_config.meta_file_path);

  // determine where to log to
  if( !strcmp(main_config.log_type, "Both") || !strcmp(main_config.log_type, "Monitor") )
  {
    output_to_monitor(main_config, mdata);
  }
  if( !strcmp(main_config.log_type, "Both") || !strcmp(main_config.log_type, "File") )
  {
    output_to_log(main_config, mdata);
  }

  return 0;
}

config read_config(char filename[MAX_LEN])
{
  FILE *file;
  config configuration;
  char *buffer;
  char valid_head[MAX_LEN] = "Start Simulator Configuration File\n";
  char valid_tail[MAX_LEN] = "End Simulator Configuration File";

  // allocate memory to read in each line
  buffer = (char *) malloc(MAX_LEN);

  // open the config file
  file = fopen(filename, "r");
  get_line(file, buffer, ':');

  // verify header
  if( strcmp(buffer, valid_head) )
  {
    printf("[ERROR] Invalid configuration file. Exit 1.\n");
    exit(1);
  }

  // continue reading in the rest of the file
  // look for specific key phrases
  while( get_line(file, buffer, ':') != EOF && strcmp(buffer, valid_tail) != 0 )
  {
    if( strcmp(buffer, "Version/Phase:") == 0 )
    {
      fscanf (file, "%f", &configuration.version);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "File Path:") == 0 )
    {
      fscanf (file, "%s", configuration.meta_file_path);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Processor cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.processor_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Monitor display time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.monitor_display_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Hard drive cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.hard_drive_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Printer cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.printer_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Keyboard cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.keyboard_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Memory cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.memory_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "System memory (kbytes):") == 0 )
    {
      fscanf (file, "%d", &configuration.physical_memory);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Log:") == 0 )
    {
      fscanf (file, "%s", configuration.log_type); // read over the first two words
      fscanf (file, "%s", configuration.log_type);
      fscanf (file, "%s", configuration.log_type); // this is the word we want

      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Log File Path:") == 0 )
    {
      fscanf (file, "%s", configuration.log_path);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else
    {
      printf("Unknown configuration value: '%s' Skipping...\n", buffer);
      get_line(file, buffer, '\n'); // move on to the next line
    }

  }
  fclose(file);
  free(buffer);

  return configuration;
}

void read_data(meta_data *mdata, char filename[MAX_LEN])
{
  FILE *file;
  char *buffer;
  char *cat_buffer;
  char peek;
  int buffer_len;
  int index = 0;
  char valid_head[MAX_LEN] = "Start Program Meta-Data Code:\n";

  // allocate memory to read in each line
  buffer = (char *) malloc(MAX_LEN);
  cat_buffer = (char *) malloc(MAX_LEN);

  // open the config file
  file = fopen(filename, "r");
  get_line(file, buffer, ';');

  // verify header
  if( strcmp(buffer, valid_head) )
  {
    printf("[ERROR] Invalid meta-data file. Exit 1.\n");
    exit(1);
  }

  // read in one string at a time
  while( fscanf(file, "%s", buffer) != EOF && strcmp(buffer, "End") )
  {
    buffer_len = strlen(buffer); // set string length for terminator checking
    peek = getc(file); // peek at the next character

    // check for the following:
    //   * accepted terminators/delimeters are not present
    //     * this includes periods, colons, and semi-colons
    //   * also check for a space after the string
    // if both conditions are met, then chances are we have a multi-word string
    // to read in, thus we should append the following string to the current buffer
    if( buffer[buffer_len - 1] != ':' && buffer[buffer_len - 1] != ';'
        && buffer[buffer_len - 1] != '.' && peek == ' '                )
    {
      fscanf(file, "%s", cat_buffer); // read in the next string
      buffer[buffer_len] = ' '; // replace the current terminator with a space
      buffer[buffer_len + 1] = '\0'; // append a terminator after to recreate string
      strcat(buffer, cat_buffer); // concatenate the two strings
    }
    else
    {
      ungetc(peek, file); // put the "peeked" character back if the above case didn't occur
    }

    mdata[index].code = get_code(buffer);
    get_descriptor(buffer, mdata[index].descriptor);
    mdata[index].cycles = get_cycles(buffer);

    index++;
  }

  fclose(file); // close the file

  free(buffer);
  free(cat_buffer);

  return;
}

// function to get meta data code
char get_code(char* string)
{
    if( string == NULL )
    {
      return -1;
    }
    else
    {
      return string[0];
    }
}

// function to get descriptor
int get_descriptor(char* string, char* descriptor)
{
  int string_index = 2; // start at the 3rd letter
  int descriptor_index = 0; // start at the first location

  if( string == NULL )
  {
    return -1;
  }

  // read until descriptor delim
  while( string[string_index] != ')' && string_index < MAX_LEN )
  {
    descriptor[descriptor_index] = string[string_index];
    string_index++;
    descriptor_index++;
  }

  descriptor[descriptor_index] = '\0';
  return 0;
}

// function to get an integer # of cycles from a string
int get_cycles(char* string)
{
    int index = 0;
    int cycles_index = 0;
    int size = 0;
    int cycles[MAX_LEN]; // that's a big number
    int total = 0;

    if( string == NULL )
    {
      return -1;
    }

    // loop until the expected cycles location
    while( string[index] != ')' )
    {
      index++;
    }
    index++;

    // loop over all consecutive available numbers
    while( string[index] >= '0' && string[index] <= '9' )
    {
      cycles[cycles_index] = string[index] - '0';
      index++;
      cycles_index++;
    }
    size = cycles_index;
    cycles_index--;

    // convert int array into integer
    for( index = 0; index < size; index++)
    {
      total += cycles[index] * power(10, cycles_index);
      cycles_index--;
    }

    return total;
}

// function to calculate exponents
int power(int base, int power)
{
  int index;

  if( power == 0 )
  {
    return 1;
  }

  for( index = 1; index < power; index++ )
  {
      base *= base;
  }

  return base;
}

// function to log to file
void output_to_log(config configuration, meta_data *mdata)
{
  int index = 0;
  int run_time = 0;
  FILE* file;

  file = fopen(configuration.log_path, "w+");

  fprintf(file, "Configuration File Data\n");
  fprintf(file, "Processor = %d ms/cycle\n", configuration.processor_cycle_time);
  fprintf(file, "Monitor = %d ms/cycle\n", configuration.monitor_display_time);
  fprintf(file, "Hard Drive = %d ms/cycle\n", configuration.hard_drive_cycle_time);
  fprintf(file, "Printer = %d ms/cycle\n", configuration.printer_cycle_time);
  fprintf(file, "Keyboard = %d ms/cycle\n", configuration.keyboard_cycle_time);
  fprintf(file, "Memory = %d ms/cycle\n", configuration.memory_cycle_time);

  if( !strcmp(configuration.log_type, "Both") )
  {
    fprintf(file, "Logged to: monitor and %s\n\n", configuration.log_path);
  }
  else
  {
    fprintf(file, "Logged to: %s\n\n", configuration.log_path);
  }

  // loop until system end call
  while( !(mdata[index].code == 'S' && !strcmp(mdata[index].descriptor, "end")))
  {
    switch(mdata[index].code)
    {
      case 'P':
        run_time = configuration.processor_cycle_time * mdata[index].cycles;
        break;

      case 'M':
        run_time = configuration.memory_cycle_time * mdata[index].cycles;
        break;

      case 'I':
        if( !strcmp(mdata[index].descriptor, "keyboard"))
        {
          run_time = configuration.keyboard_cycle_time * mdata[index].cycles;
        }
        else
        {
          run_time = configuration.hard_drive_cycle_time * mdata[index].cycles;
        }
        break;

      case 'O':
        if( !strcmp(mdata[index].descriptor, "monitor"))
        {
          run_time = configuration.monitor_display_time * mdata[index].cycles;
        }
        else
        {
          run_time = configuration.printer_cycle_time * mdata[index].cycles;
        }
        break;

      default:
        run_time = 0;
        break;
    }

    if( run_time != 0 )
    {
      fprintf(file, "%c(%s)%d - %d ms\n", mdata[index].code, mdata[index].descriptor, mdata[index].cycles, run_time);
    }
    index++;
  }

  fclose(file);
}

// function to output to monitor
void output_to_monitor(config configuration, meta_data *mdata)
{
  int index = 0;
  int run_time = 0;

  printf("Configuration File Data\n");
  printf("Processor = %d ms/cycle\n", configuration.processor_cycle_time);
  printf("Monitor = %d ms/cycle\n", configuration.monitor_display_time);
  printf("Hard Drive = %d ms/cycle\n", configuration.hard_drive_cycle_time);
  printf("Printer = %d ms/cycle\n", configuration.printer_cycle_time);
  printf("Keyboard = %d ms/cycle\n", configuration.keyboard_cycle_time);
  printf("Memory = %d ms/cycle\n", configuration.memory_cycle_time);

  if( !strcmp(configuration.log_type, "Both") )
  {
    printf("Logged to: monitor and %s\n\n", configuration.log_path);
  }
  else
  {
    printf("Logged to: monitor\n\n");
  }

  // loop until system end call
  while( !(mdata[index].code == 'S' && !strcmp(mdata[index].descriptor, "end")))
  {
    switch(mdata[index].code)
    {
      case 'P':
        run_time = configuration.processor_cycle_time * mdata[index].cycles;
        break;

      case 'M':
        run_time = configuration.memory_cycle_time * mdata[index].cycles;
        break;

      case 'I':
        if( !strcmp(mdata[index].descriptor, "keyboard"))
        {
          run_time = configuration.keyboard_cycle_time * mdata[index].cycles;
        }
        else
        {
          run_time = configuration.hard_drive_cycle_time * mdata[index].cycles;
        }
        break;

      case 'O':
        if( !strcmp(mdata[index].descriptor, "monitor"))
        {
          run_time = configuration.monitor_display_time * mdata[index].cycles;
        }
        else
        {
          run_time = configuration.printer_cycle_time * mdata[index].cycles;
        }
        break;

      default:
        run_time = 0;
        break;
    }

    if( run_time != 0 ) // don't print out actions with a time of zero
    {
      printf("%c(%s)%d - %d ms\n", mdata[index].code, mdata[index].descriptor, mdata[index].cycles, run_time);
    }
    index++;
  }
}
