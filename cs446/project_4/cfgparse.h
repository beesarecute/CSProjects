/**************************************************************************/
// Description: Library for handling config file operations
// Author: Eric Olson
// Version: 1.2
// Date Last Modified: 12/15/16
/**************************************************************************/

#ifndef MAX_SIZE
#define MAX_SIZE 1024
#endif // MAX_SIZE

#ifndef CFGPARSE_H_
#define CFGPARSE_H_

#include <stdio.h>
#include "file.h"

typedef struct
{
  int processor_quantum;
  int processor_cycle_time;
  int monitor_display_time;
  int hard_drive_cycle_time;
  int printer_cycle_time;
  int keyboard_cycle_time;
  int memory_cycle_time;

  int physical_memory;
  int block_size;
  int allocated_blocks;
  
  int num_printers;
  int num_hdds;

  float version;
  char processor_scheduling_code[MAX_SIZE];
  char meta_file_path[MAX_SIZE];
  char log_type[MAX_SIZE];
  char log_path[MAX_SIZE];
} config;

config read_config(char filename[MAX_SIZE]);

#endif // cfgparse_h_
