#include "timer.h"

// Function to wait a specified time in accordance with the system clock
// Input:   Integer wait time in milliseconds,
// Outcome: Time since program was started is returned in seconds
//
double timestamp(int wait_time_ms)
{
  double wait_time_s, stamp_time;
  clock_t current_time, end_time;

  wait_time_s = (double)(wait_time_ms) / 1000; // convert ms to s
  current_time = clock(); // capture current cycles
  end_time = current_time + (wait_time_s * CLOCKS_PER_SEC); // set end cycles

  while( current_time < end_time ) // wait until end cycles
  {
    current_time = clock();
  }

  stamp_time = (double)(current_time) / CLOCKS_PER_SEC; // convert cycles to seconds

  return stamp_time;
}
