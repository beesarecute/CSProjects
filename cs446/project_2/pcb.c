#include "pcb.h"

// Function to create a process
// Input:   PCB to create the process into
//          Meta data to generate process information from
//          Integer pid of the calling process
//          Integer representing system memory available to the process
// Outcome: PCB is passed back by reference
//          PID of the created PCB is returned
//
int p_create(pcb *process, meta_data mdata, int p_num, int memory)
{
  strcpy((*process).state, "ENTER");
  (*process).p_mdata = mdata;
  (*process).sys_memory = memory;

  // if the meta data is attempting to start a new application, increment the pid
  if( (*process).p_mdata.code == 'A' && !strcmp((*process).p_mdata.descriptor, "start") )
  {
    p_num++;
  }
  (*process).pid = p_num;

  return (*process).pid;
}

// Function to change the process state to ready
// Input:  PCB to change the state of
// Outcome: PCB is passed back by reference
//
void p_ready(pcb *process)
{
  strcpy((*process).state, "READY");
}

// Function to run a process
// Input:   PCB to run
//          Configuration to determine log path
//          File stream to log to if specified
// Outcome: Process runs based on instructions stored in PCB
//          and the results are logged in the specified location(s)
//
void p_run(pcb *process, config configuration, FILE* fstream)
{
  pthread_t thread;
  char output[MAX_SIZE];
  static double start_time; // used to store time of when os itself started
                            // using this makes the time output less accurate
                            // in terms of total program run time, but instead
                            // records the total "OS" run time

  // change process state to running
  strcpy((*process).state, "RUNNING");

  // determine path based on process code
  // then run process for specified time and store output into array
  switch( (*process).p_mdata.code )
  {
    case 'S':
      if( !strcmp((*process).p_mdata.descriptor, "start") )
      {
        start_time = timestamp(0);
      }
      sprintf(output, "%lf - Simulator program %sing\n", timestamp((*process).p_mdata.run_time) - start_time, (*process).p_mdata.descriptor);
      break;

    case 'A':
      if( !strcmp((*process).p_mdata.descriptor, "start") )
      {
        sprintf(output, "%lf - OS: preparing process %d\n", timestamp(0) - start_time, (*process).pid);
        sprintf(output, "%lf - OS: starting process %d\n", timestamp((*process).p_mdata.run_time) - start_time, (*process).pid);
      }
      else if( !strcmp((*process).p_mdata.descriptor, "end") )
      {
        sprintf(output, "%lf - OS: removing process %d\n", timestamp(0) - start_time, (*process).pid);
      }
      break;

    case 'P':
      sprintf(output, "%lf - Process %d: start processing action\n", timestamp(0) - start_time, (*process).pid);
      sprintf(output, "%lf - Process %d: end processing action\n", timestamp((*process).p_mdata.run_time) - start_time, (*process).pid);
      break;

    case 'I':
      sprintf(output, "%lf - Process %d: start %s input\n", timestamp(0) - start_time, (*process).pid, (*process).p_mdata.descriptor);
      pthread_create(&thread, NULL, t_run, process);
      pthread_join(thread, NULL);
      sprintf(output, "%lf - Process %d: end %s input\n", timestamp(0) - start_time, (*process).pid, (*process).p_mdata.descriptor);
      break;

    case 'O':
      sprintf(output, "%lf - Process %d: start %s output\n", timestamp(0) - start_time, (*process).pid, (*process).p_mdata.descriptor);
      pthread_create(&thread, NULL, t_run, process);
      pthread_join(thread, NULL);
      sprintf(output, "%lf - Process %d: end %s output\n", timestamp(0) - start_time, (*process).pid, (*process).p_mdata.descriptor);
      break;

    case 'M':
      if( !strcmp((*process).p_mdata.descriptor, "allocate") )
      {
        sprintf(output, "%lf - Process %d: allocating memory\n", timestamp(0) - start_time, (*process).pid);
        sprintf(output, "%lf - Process %d: memory allocated at 0x%d\n", timestamp((*process).p_mdata.run_time) - start_time, (*process).pid, AllocateMemory((*process).sys_memory));
      }
      else if( !strcmp((*process).p_mdata.descriptor, "cache") )
      {
        sprintf(output, "%lf - Process %d: start memory caching\n", timestamp(0) - start_time, (*process).pid);
        sprintf(output, "%lf - Process %d: end memory caching\n", timestamp((*process).p_mdata.run_time) - start_time, (*process).pid);
      }
      break;

    default:
      sprintf(output, "[ERROR] Unknown code '%c' invoked. Skipping.\n", (*process).p_mdata.code);
      break;
  }

  // determine whether to print to monitor
  if( !strcmp(configuration.log_type, "Monitor") || !strcmp(configuration.log_type, "Both") )
  {
    printf("%s", output);
  }

  // determine whether to print to file
  if( !strcmp(configuration.log_type, "File") || !strcmp(configuration.log_type, "Both") )
  {
    fprintf(fstream, "%s", output);
  }

}

// Function for running thread
// Input:   Void pointer, usually a pcb
// Outcome: Thread is ran for given time specified in PCB
//
void* t_run(void* arg)
{
    pcb *process;

    process = (pcb *) arg;
    timestamp(process->p_mdata.run_time);

    // return 42, of course
    return (void *) 42;
}
