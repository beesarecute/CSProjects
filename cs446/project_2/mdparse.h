/**************************************************************************/
// Description: Library for handling meta data operations
// Author: Eric Olson
// Version: 1.0
// Date Last Modified: 10/5/16
/**************************************************************************/

#ifndef MAX_SIZE
#define MAX_SIZE 1024
#endif // MAX_SIZE

#ifndef MDPARSE_H_
#define MDPARSE_H_

#include <stdio.h>
#include "cfgparse.h"
#include "file.h"

typedef struct
{
  char code;
  char descriptor[MAX_SIZE];
  int cycles;
  int run_time;
} meta_data;

void read_data(meta_data *data, config configuration);
char get_code(char* string);
int get_descriptor(char* string, char* descriptor);
int get_cycles(char* string);
int get_run_time(config configuration, meta_data mdata);
int power(int base, int power);

#endif // MDPARSE_H_
