/**************************************************************************/
// Description: Library for handling everything process control block
// Author: Eric Olson
// Version: 1.0
// Date Last Modified: 10/6/16
/**************************************************************************/

#ifndef PCB_H_
#define PCB_H_

#include <pthread.h>
#include <stdio.h>
#include "cfgparse.h"
#include "mdparse.h"
#include "timer.h"
#include "SimulatorFunctions.h"

typedef struct {
  int pid;
  int sys_memory;
  meta_data p_mdata;
  char state[MAX_SIZE];
} pcb;

int p_create(pcb *process, meta_data, int p_num, int memory);
void p_ready(pcb *process);
void p_run(pcb *process, config configuration, FILE* fstream);
void* t_run(void* arg);

#endif // PCB_H_
