#include "file.h"

// Function to get a line from a file up to specified delimeter or newline
// Input:   File stream,
//          Char* to store line
//          Delimeter to stop at
// Outcome: Line is passed back by reference
//          Boolean is returned to represent success (whether EOF was hit)
//
int get_line(FILE *stream, char *line, char delim)
{
  int index = 0;
  memset(line, 0, MAX_SIZE); // flush array
  fflush(stream); // flush file stream

  line[index] = getc(stream); // get character at cursor

  // check if we are already at end of file
  if( line[index] == EOF )
  {
    return -1;
  }

  // continue grabbing characters until one of the following occurs:
  //   * character limit is reached
  //   * specified delimeter is hit
  //   * newline character is hit
  while( index < MAX_SIZE && line[index] != delim && line[index] != '\n' )
  {
    index++;
    line[index] = getc(stream);

    if( line[index] == EOF )
    {
      line[index] = '\0'; // drop the stored EOF character
      break; // break to conserve current string
    }
  }

  return 0;
}

// Function to check if a specified file exists
// Input:   Filename to check
// Outcome: Returns boolean value depending on if the file exists
//          (and/or has the correct permissions)
//
int file_exists(char filename[MAX_SIZE])
{
  if( access( filename, F_OK ) != -1 )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}
