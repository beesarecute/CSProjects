#include "../include/ip.h"

ip::ip() {
  first_octet = 0;
  second_octet = 0;
  marked = false;
  address = "000.000";
}

ip::~ip() {
  clear();
}

ip ip::insert(int first, int second) {
  std::string first_s, second_s;

  first_octet = first;
  second_octet = second;

  first_s = std::to_string(first_octet);
  second_s = std::to_string(second_octet);

  address = first_s + "." + second_s;

  return *this;
}

ip ip::generate() {
  int first, second;

  first = std::rand() % 256;
  second = std::rand() % 256;
  insert(first, second);

  return *this;
}

void ip::clear() {
  first_octet = 0;
  second_octet = 0;
  marked = false;
  address = "000.000";
}

int ip::get_first_octet() {
  return first_octet;
}

int ip::get_second_octet() {
  return second_octet;
}

bool ip::is_marked() {
  return marked;
}

std::string ip::get_address() {
  return address;
}

void ip::operator = (const ip &net) {
  first_octet = net.first_octet;
  second_octet = net.second_octet;
  address = net.address;
}