#include "../include/ip.h"

int main() {
  ip ip1;
  std::cout << "constructed ip: " << ip1.get_address() << std::endl;

  ip1.insert(123, 12);
  std::cout << "inserted ip (123, 12): " << ip1.get_address() << std::endl;

  ip1.generate();
  std::cout << "generated ip: " << ip1.get_address() << std::endl;

  ip1.clear();
  std::cout << "cleared ip: " << ip1.get_address() << std::endl;

  return 0;
}