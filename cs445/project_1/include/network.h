#ifndef CLASS_NETWORK_H
#define CLASS_NETWORK_H

#include <iostream>
#include "ip.h"

class Network {
  public:
    Network();
    ~Network();

    void insert(ip new_host);
    bool remove();
    void clear();

    int get_size();

    bool next();
    bool previous();
    void print();

  private:
    int branches;
    int size;
    int current;
    ip* hosts;
};

#endif // CLASS_NETWORK_H