#!/usr/bin/env python
import sys
import time
import random
from random import randint
from scapy.all import *

conf.verb=0

if len(sys.argv) != 3:
    print "Invalid number of arguments"
    sys.exit(1)

source = sys.argv[1]
target = sys.argv[2]

ports = [20,21,22,23,25,37,53,69,80,443]
random.shuffle(ports)

for index in range(0, 10):
    src_port = randint(1,65535)
    dst_port = ports[index]
    sleep_time = randint(0, 5)

    time.sleep(sleep_time)
    packet=sr1(IP(dst=target,src=source)/TCP(dport=dst_port,sport=src_port,flags="FPU"),timeout=10)

    if(str(type(packet))=="<type 'NoneType'>"):
        print "Port", dst_port, "is open/filtered"
    elif(packet.getlayer(TCP).flags == 0x14):
        print "Port", dst_port, "is closed"
    elif(packet.haslayer(ICMP)):
        if(int(packet.getlayer(ICMP).type)==3 and int(packet.getlayer(ICMP).code) in [1,2,3,9,10,13]):
            print "Port", dst_port, "is filtered"

sys.exit(0)
