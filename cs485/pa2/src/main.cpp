#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <cstring>

extern void ReadImage(char[], int ***, int&, int&, int&);
extern void WriteImage(char[], int **, int, int, int);
extern "C" void solve_system(int, int, float**, float*, float*);

int set_actual(float **a, char *filename);

int main(int argc, char* argv[]) {
  int*** fimage;
  int j = 4,k = 4;
  int m,n,q;

  float **a, *x, *b;

  fimage = new int** [1];
  a = new float* [j+1];
  for ( int i = 0; i < j+1; i++ ) {
    a[i] = new float [k+1];
  }

  x = new float [k+1];
  b = new float [j+1];

  if ( argc != 3 ) {
    std::cerr << "main: invalid number of arguments" << std::endl;
    return 1;
  }

  ReadImage(argv[1], fimage, m, n, q);

  if ( set_actual(a, argv[1]) ) {
    std::cerr << "main: set_actual returned an error code" << std::endl;
  }

  b[1] = 10;
  b[2] = 20;
  b[3] = 15;
  b[4] = 15;

  solve_system(j,k,a,x,b);
  std::cout << x[2] << " ";

  // shift x
  for ( int i = 0; i < m; i++ ) {
    if ( x[2] < 0 ) {
      fimage[0][i] += int(40 + x[2]);
    } else {
      fimage[0][i] += int(x[2]);
    }
  }

  b[1] = 10;
  b[2] = 10;
  b[3] = 20;
  b[4] = 30;

  solve_system(j,k,a,x,b);
  std::cout << x[2] << std::endl;
  
  //shift y
  if ( x[2] < 0 ) {
    *fimage += int(48 + x[2]);
  } else {
    *fimage += int(x[2]);
  }

  WriteImage(argv[2], *fimage, 40, 48, q);

  for ( int i = 0; i < j+1; i++ ) {
    delete [] a[i];
  }
  delete [] a;
  delete [] x;
  delete [] b;
}

int set_actual(float **a, char *filename) {
  if( !strcmp(filename, "input/1.pgm") ) {
    a[1][0] = 26.9;
    a[1][1] = 50.9;
    a[1][2] = 1;
    a[2][0] = 63.5;
    a[2][1] = 50.5;
    a[2][2] = 1;
    a[3][0] = 44.9;
    a[3][1] = 70.4;
    a[3][2] = 1;
    a[4][0] = 44.4;
    a[4][1] = 87.8;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/2.pgm") ) {
    a[1][0] = 32.9;
    a[1][1] = 44.4;
    a[1][2] = 1;
    a[2][0] = 75.3;
    a[2][1] = 43.3;
    a[2][2] = 1;
    a[3][0] = 63.5;
    a[3][1] = 64.4;
    a[3][2] = 1;
    a[4][0] = 58.2;
    a[4][1] = 86.2;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/3.pgm") ) {
    a[1][0] = 25.1;
    a[1][1] = 48.7;
    a[1][2] = 1;
    a[2][0] = 62.0;
    a[2][1] = 45.1;
    a[2][2] = 1;
    a[3][0] = 45.3;
    a[3][1] = 70.9;
    a[3][2] = 1;
    a[4][0] = 44.7;
    a[4][1] = 89.3;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/4.pgm") ) {
    a[1][0] = 15.7;
    a[1][1] = 45.3;
    a[1][2] = 1;
    a[2][0] = 55.3;
    a[2][1] = 42.0;
    a[2][2] = 1;
    a[3][0] = 28.7;
    a[3][1] = 61.3;
    a[3][2] = 1;
    a[4][0] = 33.7;
    a[4][1] = 83.3;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/5.pgm") ) {
    a[1][0] = 36.0;
    a[1][1] = 44.0;
    a[1][2] = 1;
    a[2][0] = 76.0;
    a[2][1] = 42.3;
    a[2][2] = 1;
    a[3][0] = 68.3;
    a[3][1] = 61.0;
    a[3][2] = 1;
    a[4][0] = 64.3;
    a[4][1] = 84.0;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/6.pgm") ) {
    a[1][0] = 13.5;
    a[1][1] = 47.0;
    a[1][2] = 1;
    a[2][0] = 52.8;
    a[2][1] = 44.2;
    a[2][2] = 1;
    a[3][0] = 27.2;
    a[3][1] = 63.8;
    a[3][2] = 1;
    a[4][0] = 30.0;
    a[4][1] = 85.0;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/7.pgm") ) {
    a[1][0] = 24.7;
    a[1][1] = 44.7;
    a[1][2] = 1;
    a[2][0] = 61.3;
    a[2][1] = 42.7;
    a[2][2] = 1;
    a[3][0] = 44.0;
    a[3][1] = 57.0;
    a[3][2] = 1;
    a[4][0] = 44.3;
    a[4][1] = 80.7;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/8.pgm") ) {
    a[1][0] = 28.0;
    a[1][1] = 45.8;
    a[1][2] = 1;
    a[2][0] = 65.0;
    a[2][1] = 43.2;
    a[2][2] = 1;
    a[3][0] = 48.5;
    a[3][1] = 63.8;
    a[3][2] = 1;
    a[4][0] = 49.2;
    a[4][1] = 84.0;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/9.pgm") ) {
    a[1][0] = 31.2;
    a[1][1] = 36.5;
    a[1][2] = 1;
    a[2][0] = 68.0;
    a[2][1] = 35.0;
    a[2][2] = 1;
    a[3][0] = 53.5;
    a[3][1] = 46.5;
    a[3][2] = 1;
    a[4][0] = 52.0;
    a[4][1] = 72.8;
    a[4][2] = 1;
  } else if( !strcmp(filename, "input/10.pgm") ) {
    a[1][0] = 35.5;
    a[1][1] = 52.2;
    a[1][2] = 1;
    a[2][0] = 75.0;
    a[2][1] = 49.2;
    a[2][2] = 1;
    a[3][0] = 62.8;
    a[3][1] = 74.5;
    a[3][2] = 1;
    a[4][0] = 57.0;
    a[4][1] = 90.5;
    a[4][2] = 1;
  } else {
    std::cerr << "set_actual: coordinates for file not found" << std::endl;
    return 1;
  }

  return 0;
}
