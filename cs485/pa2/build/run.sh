#!/bin/bash

BIN_PATH=../bin

if [[ ! -f ${BIN_PATH}/main ]]; then
  echo "no executable found. attempting to build..."
  make
fi

if [[ $? != 0 ]]; then
  echo "build failed :("
  exit 1
fi

cd ..

if [[ ! -d "output" ]]; then
  mkdir output
fi

for i in `seq 1 10`; do
  ./bin/main input/${i}.pgm output/${i}out.pgm
done