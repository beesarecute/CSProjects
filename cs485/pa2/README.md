# HW 2

## Building/compiling
All of the necessary shared object files and executables can be created using the makefile provided. To do so, simply type:
```
$ cd build
$ make
```

This will create all of the necessary files in the **bin** folder. Conversely, to delete all of the compiled files, type:
```
$ cd build
$ make clean
```

## Running
The easiest way to run the program is to use the bash script provided **run.sh** located in the build folder. To use it, type:
```
$ cd build
$ chmod +x run.sh
$ ./run.sh
```
If ran this way, the resulting image files can be found in the **output** directory.

To run the program without using the bash script, two command-line arguments will need to be provided (the input file, and the output file). E.g.
```
$ ./bin/main input/1.pgm output/1out.pgm
```
