nfa -> dfa conversion rules
  1. create graph Gd with vertex {q0} = initial vertex
  2. repeat for all edges:
    a. take any vertex {qi, ..., qk} of Gd that has no out-going edge
       for some symbol in sigma
    b. compute extended delta function (from your nfa) for all edges
    c. form union from the results of step b
    d. create new vertex for Gd, label {ql, qm, ..., qn}
    e. add to Gd an edge from step b to step d
  3. every state in Gd with label that contains a final state from
     nfa, make final state in dfa
  4. if Mn accepts lamda, make {q0} in Gd a final vertex
